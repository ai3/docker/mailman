"""Display the last post time for a list or all lists.

Save as bin/last_post.py

Run via

    bin/withlist -r last_post listname

or

    bin/withlist -a -r last_post

to do all lists.
"""


import time


def last_post(mlist):
    print '%s@%s: Created at %s - Last post at %s' % (
        mlist.real_name, mlist.host_name, 
        time.ctime(mlist.created_at),
        time.ctime(mlist.last_post_time)
        )
