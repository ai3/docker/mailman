# Locked to buster due to Python 2 requirement.
FROM registry.git.autistici.org/ai3/docker/apache2-base:s6-buster

COPY conf /etc/
COPY build.sh /tmp/build.sh
COPY apply-forced-params /usr/local/bin/apply-forced-params
COPY clean-heldmsg /usr/local/bin/clean-heldmsg
COPY sendmail.py /usr/sbin/sendmail
RUN /tmp/build.sh && rm -fr /tmp/build.sh /tmp/conf
COPY last_post.py /var/lib/mailman/bin/last_post.py
COPY newlist.en /var/lib/mailman/templates/en/newlist.txt
