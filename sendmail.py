#!/usr/bin/python
#
# Send an email sent from stdin using a remote SMTP server.
# Behaves similarly to /usr/sbin/sendmail (sort of).
#

import optparse
import sys
import smtplib


parser = optparse.OptionParser()
parser.add_option('-i', dest='ignored', action='store_true')
parser.add_option('-f', dest='from_addr', default='nobody')
parser.add_option('--server', dest='server', default='localhost')
parser.add_option('--port', dest='server_port', type='int', default=25)
opts, args = parser.parse_args()

if not args:
    raise Exception('Not enough arguments')

msg = sys.stdin.read()
s = smtplib.SMTP(opts.server, opts.server_port)
s.sendmail(opts.from_addr, args, msg)
s.quit()

