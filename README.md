docker-mailman
===

Docker container for Mailman 2.

It runs along with an Apache instance.

The /data volume should be mounted externally (with the appropriate
permissions) and it will be used to store lists, archives and all
the other Mailman runtime data.

The /config volume should also be mounted externally, and it should
contain the *mm_cfg.py* and the rest of the configuration files.
