#!/bin/sh
#
# Install script for Mailman 2 inside a Docker container.
#

# Packages that are only used to build the site. These will be
# removed once we're done.
BUILD_PACKAGES="
    build-essential
    python-dev
    git
"

# Packages required to serve the website and run the services, in
# addition to those already installed by the base apache2 image.
PACKAGES="
    ai-sso
    ca-certificates
    mailman
    locales
    python-pip
    python-setuptools
    python-flask
    python-wheel
    findutils
"

# Apache modules to enable.
APACHE_MODULES_ENABLE="
    cgi
"

# Sites to enable.
APACHE_SITES="
    mailman
"

PYTHON_PIP_PACKAGES="
    git+https://git.autistici.org/ai/sso.git#egg=sso&subdirectory=src/python
    git+https://git.autistici.org/ai3/tools/python-mailman-api.git#egg=mailman-api
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qqy --no-install-recommends "$@"
    }
fi

set -e

# Preseed debconf for mailman. Install all languages.
debconf-set-selections <<EOF
mailman mailman/default_server_language select en
mailman mailman/site_languages multiselect ar (Arabic), ca (Catalan), cs (Czech), da (Danish), de (German), en (English), es (Spanish), et (Estonian), eu (Basque), fi (Finnish), fr (French), hr (Croatian), hu (Hungarian), ia (Interlingua), it (Italian), ja (Japanese), ko (Korean), lt (Lithuanian), nl (Dutch), no (Norwegian), pl (Polish), pt (Portuguese), pt_BR (Brasilian Portuguese), ro (Romanian), ru (Russian), sl (Slovenian), sr (Serbian), sv (Swedish), tr (Turkish), uk (Ukrainian), vi (Vietnamese), zh_CN (Chinese - China), zh_TW (Chinese - Taiwan)
EOF

# Always prefer mailman from ai3
cat > /etc/apt/preferences.d/90mailman <<EOF
Package: mailman
Pin: release n=float/buster
Pin-Priority: 1001
EOF

apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES}

# Setup Apache.
a2enmod -q ${APACHE_MODULES_ENABLE}
a2ensite ${APACHE_SITES}

# Create writable storage mountpoint. The situation with /var/lib/mailman
# is complex, there are "data" files and mailman files, and Debian links
# various subdirectories to locations in the /usr filesystem.
#
# Instead, we're going to do the reverse, and replace the "data" subdirs
# in /var/lib/mailman with symbolic links to /data, which will be where
# the data volume will be mounted in the container.
mkdir -p /data

# Exclude 'messages': it is maintained by the Debian package.
mailman_data_subdirs="archives data lists locks logs qfiles spam"
for d in $mailman_data_subdirs ; do
    rm -fr /var/lib/mailman/$d || true
    ln -sf /data/$d /var/lib/mailman/$d
done

# We still have to add the images, those live in /usr/share/images/mailman.
ln -sf /usr/share/images/mailman /data/icons

# Since Mailman installs stuff in /etc/mailman too, we do the same trick
# as above but with the /config mountpoint.
mkdir -p /config
rm -f /etc/mailman/mm_cfg.py
for f in mm_cfg.py mailman_api.conf domains forced_params ; do
    ln -s /config/$f /etc/mailman/$f
done

# Create directory for PID file.
mkdir /var/run/mailman
chmod 1777 /var/run/mailman

# We don't need the setgid bit on Mailman CGI scripts.
# Everything runs as the same user in the container, including
# the web server.
chmod g-s /usr/lib/cgi-bin/mailman/*

# Create empty dir for webroot.
mkdir /var/empty

# Ensure that all our scripts are executable
chmod 755 /usr/local/bin/apply-forced-params \
          /usr/local/bin/clean-heldmsg \
          /usr/sbin/sendmail

# Install python-mailman-api.
(cd /tmp ; for pkg in $PYTHON_PIP_PACKAGES; do pip install "$pkg"; done)

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf

